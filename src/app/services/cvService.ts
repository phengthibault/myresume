import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { catchError, retry } from "rxjs/operators";
import { Observable, throwError } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CVService {
  constructor(private firestore: AngularFirestore, private http: HttpClient) {}
  //this.afs.collection("/products").add(Object.assign(product));
  addUserTrack(data) {
    return new Promise<any>((resolve, reject) => {
      this.firestore
        .collection("userTrack")
        .add(data)
        .then(
          (res) => {},
          (err) => reject(err)
        );
    });
  }
  getdata() {
    return this.firestore.collection("userTrack").snapshotChanges(); //.valueChanges();
  }
  getIpGEOLocation() {
    console.log("look for ip");
    return this.http
      .get("https://api.ipdata.co/?api-key=test")
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
