export enum navbarField {
  experience = "EXPERIENCE",
  education = "EDUCATION",
  skills = "SKILLS",
}

export const skills = {
  item: [
    { name: "Spring", imgpath: "assets/spring.png" },
    { name: "Java", imgpath: "assets/Java.jpg" },
    { name: "Angular", imgpath: "assets/angular.png" },
  ],
};

export const personalInfo = {
  name: "PHENG",
  firstname: "Thibault",
  presentation:
    "Hello! I'm currently an intern at Worldine, I had studied at CPE Lyon computer science and I'm  familiar with several computer languages please find below my contact details and don't hesitate to send me an email or connect via Linkedin",
  experience: [
    {
      dateBegin: "Feb 2020",
      dateEnd: "July 2020",
      compagny: "Worldline",
      title: "Software Engineer Intern :Chatbot Development",
      location: "Villeurbanne, France",
      img: "assets/eW.png",
    },
    {
      dateBegin: "Jul 2018",
      dateEnd: "Jul 2019",
      compagny: "Hilti",
      title: "Software Engineer Intern: Matlab data analysis",
      location: "Schaan ,Liechtenstein",
      img: "assets/Hilti.png",
    },
  ],
};
