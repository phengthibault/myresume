import { Component, OnInit, AfterViewInit } from "@angular/core";
import { navbarField } from "./data/info";
import { CVService } from "./services/cvService";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit, AfterViewInit {
  experience = navbarField.experience;
  education = navbarField.education;
  skills = navbarField.skills;
  title = "MyResume";
  ipaddress: string = "";
  country: string = "country";
  regionName: string = "";
  urlImg: string = "";
  data;
  numberView = 0;
  constructor(private cvService: CVService, translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang("en");

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    // translate.use("fr");
  }
  ngAfterViewInit(): void {}
  addIpDb(): void {
    this.cvService.addUserTrack(this.data).then((res) => {
      console.log(res);
      console.log("donne ajoutée");
    });
  }
  ngOnInit(): void {
    this.cvService.getIpGEOLocation().subscribe((response) => {
      this.ipaddress = response["ip"];
      // this.ipaddress = response["query"];
      this.regionName = response["region"];
      this.country = response["country_name"];
      this.urlImg = response["flag"];

      console.log(response);

      console.log(this.ipaddress);
      this.data = Object.assign(
        {},
        {
          ip: this.ipaddress,
          region: this.regionName,
          country: this.country,
          date: new Date(),
        }
      );
      this.addIpDb();
    });

    this.cvService.getdata().subscribe((response) => {
      this.numberView = response.length;
      console.log("nombre vue" + response.length);
    });
  }
}
