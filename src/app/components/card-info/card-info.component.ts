import { Component, OnInit } from "@angular/core";
import { personalInfo } from "../../data/info";
import { CVService } from "../../services/cvService";

@Component({
  selector: "app-card-info",
  templateUrl: "./card-info.component.html",
  styleUrls: ["./card-info.component.css"],
})
export class CardInfoComponent implements OnInit {
  name = personalInfo.name;
  firstName = personalInfo.firstname;
  presentation = personalInfo.presentation;
  data;
  constructor(private cvService: CVService) {}

  ngOnInit(): void {}
}
