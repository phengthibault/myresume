import { Component, OnInit } from "@angular/core";
import { personalInfo } from "src/app/data/info";
@Component({
  selector: "app-timeline",
  templateUrl: "./timeline.component.html",
  styleUrls: ["./timeline.component.css"],
})
export class TimelineComponent implements OnInit {
  alternate: boolean = true;
  toggle: boolean = true;
  color: boolean = true;
  size: number = 40;
  expandEnabled: boolean = true;
  side = "left";
  experiences = personalInfo.experience;

  constructor() {}
  entries = [];
  ngOnInit(): void {
    this.experiences.map((exp) =>
      this.addEntry(
        exp.title,
        exp.location,
        exp.dateBegin,
        exp.img,
        exp.dateEnd
      )
    );
  }

  addEntry(exp, content, dateBegin, img, dateEnd) {
    this.entries.push({
      header: exp,
      content: content,
      date: dateBegin,
      dateEnd: dateEnd,
      img: img,
    });
  }

  removeEntry() {
    this.entries.pop(); // test
  }

  onHeaderClick(event) {
    if (!this.expandEnabled) {
      event.stopPropagation();
    }
  }

  onDotClick(event) {
    console.log(event);

    if (!this.expandEnabled) {
      event.stopPropagation();
    }
  }

  onExpandEntry(expanded, index) {
    console.log(`Expand status of entry #${index} changed to ${expanded}`);
  }

  toggleSide() {
    this.side = this.side === "left" ? "right" : "left";
  }
}
