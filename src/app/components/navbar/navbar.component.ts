import { Component, OnInit } from "@angular/core";
import { navbarField } from "../../data/info";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"],
})
export class NavbarComponent implements OnInit {
  navbarField = Object.values(navbarField);
  translate: TranslateService;
  constructor(translate: TranslateService) {
    this.translate = translate;
  }

  ngOnInit(): void {}

  setLanguage(language: string): void {
    console.log("flag", language);
    this.translate.use(language);
  }
}
