import { Component, OnInit } from "@angular/core";
import { skills } from "src/app/data/info";
@Component({
  selector: "app-skills",
  templateUrl: "./skills.component.html",
  styleUrls: ["./skills.component.css"]
})
export class SkillsComponent implements OnInit {
  skills = skills.item;
  constructor() {}

  ngOnInit(): void {}
}
