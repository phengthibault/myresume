// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyCgt5ciKtDWC029Xq_ONqTMMnOYjbEAAj8",
    authDomain: "myresume-252ad.firebaseapp.com",
    databaseURL: "https://myresume-252ad.firebaseio.com",
    projectId: "myresume-252ad",
    storageBucket: "myresume-252ad.appspot.com",
    messagingSenderId: "253791527444",
    appId: "1:253791527444:web:b8dd4345b103cbfbf5bd01",
    measurementId: "G-ZEY5QN3FTT",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
